from django.conf import settings
#from accounting.utils import user_is_accounting_administrator


def social_media_dashboard(request):
    context_vars = {}
    #context_vars['is_accounting_administrator'] = user_is_accounting_administrator(request.user)
    context_vars['dashboard_test_name'] = getattr(settings, 'DASHBOARD_TEST_NAME', '')
    context_vars['dashboard_url_logout'] = settings.LOGOUT_URL
    print context_vars
    return context_vars
