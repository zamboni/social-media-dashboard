import calendar
from collections import defaultdict
import datetime

from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import ListView, TemplateView

from braces.views import LoginRequiredMixin

from social_media_planner import models as planner_models
from social_media_planner.utils import get_posts_calendar

"""
from accounting.views import AuthenticationFormView


class DashboardLoginView(AuthenticationFormView):
    template_name = 'lims_dashboard/dashboard_authentication.html'
"""

class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = \
        getattr(settings, 'DASHBOARD_HOME_TEMPLATE', 'social_media_dashboard/dashboard_home.html')

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['next_meeting'] = planner_models.Meeting.objects.get_next_meeting()

        date_from_string = self.request.GET.get('date_from')
        if date_from_string:
            date_from = datetime.datetime.strptime(date_from_string, '%Y-%m-%d').date()
        else:
            date_from = datetime.date.today()
        context['next_date_from'] = date_from + relativedelta(months=+3)
        context['previous_date_from'] = date_from + relativedelta(months=-3)
        posts_calendar = get_posts_calendar(date_from)
        context['posts_calendar'] = posts_calendar

        context['post_ideas'] = planner_models.Post.objects.filter(status__codename='idea')

        return context


class AccountSettingsView(LoginRequiredMixin, TemplateView):
    template_name = 'social_media_dashboard/account_settings.html'

    def get_context_data(self, **kwargs):
        context = super(AccountSettingsView, self).get_context_data(**kwargs)
        return context


"""
class DashboardSidebarContentView(TemplateView):
    template_name = 'lims_dashboard/dashboard_sidebar_content.html'

    def get_context_data(self, **kwargs):
        from postman.models import Message
        context = super(DashboardSidebarContentView,
                        self).get_context_data(**kwargs)
        context['user_messages'] = Message.objects.filter(
                recipient=self.request.user)[:5]
        return context


class DashboardMessagesListView(ListView):
    template_name = 'lims_dashboard/dashboard_messages_list.html'
    paginated_by = 20

    def get_queryset(self, **kwargs):
        from postman.models import Message
        return Message.objects.filter(recipient=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(DashboardMessagesListView,
                        self).get_context_data(**kwargs)
        context['user_messages'] = self.object_list
        return context


class DashboardMessageDetailsView(TemplateView):
    template_name = 'lims_dashboard/dashboard_message_details.html'

    def get(self, request, *args, **kwargs):
        from postman.models import Message
        message = Message.objects.get(id=kwargs['message_id'])
        message.read_at = datetime.datetime.now()
        message.save()
        return self.render_to_response({
            'message': message,
        })


class DashboardMessageDeleteView(TemplateView):
    template_name = ''

    def post(self, request, *args, **kwargs):
        from postman.models import Message
        message = Message.objects.get(id=request.POST.get('message_id'))
        message.delete()
        return HttpResponseRedirect(
                reverse('dashboard-messages-list'))
"""
