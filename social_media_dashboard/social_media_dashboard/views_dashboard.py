from django.views.generic import TemplateView


class SocialMediaDashboardView(TemplateView):
    template_name = 'social_media_dashboard/social_media_dashboard_home.html'
