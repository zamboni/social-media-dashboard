from __future__ import absolute_import

import datetime
import logging

from celery import shared_task
from mailer.engine import send_all


logging.basicConfig()
logger = logging.getLogger('Dashboard')



@shared_task
def send_all_mails():
    logger.warning("@@ Send all mails (%s)" %
                    datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    send_all()
