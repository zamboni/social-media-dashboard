from django.conf import settings
from django.conf.urls.static import static

from urls import *

urlpatterns += \
    [url(r'', include('django.contrib.staticfiles.urls'))] + \
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
