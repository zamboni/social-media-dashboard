"""social_media_dashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', views.DashboardView.as_view(), name='home'),

    url(r'^accounts/settings/$', views.AccountSettingsView.as_view(), name='account_settings'),
    url(r'^accounts/login/$', auth_views.login, name='login',
        kwargs={'template_name': 'social_media_dashboard/login_form.html'}),
    url(r'^accounts/logout/$', auth_views.logout, name='logout', kwargs={'next_page': '/'}),
    url(r'^accounts/password-change/$', auth_views.password_change, name='account_password_change',
        kwargs={
            'template_name': 'social_media_dashboard/password_change_form.html',
            'post_change_redirect': 'account_password_change_done',
        }
    ),
    url(r'^accounts/password-change/done/$', auth_views.password_change_done,
        name='account_password_change_done',
        kwargs={'template_name': 'social_media_dashboard/password_change_done.html',}
    ),

    url(r'^planner/', include('social_media_planner.urls', namespace='social_media_planner')),
]
