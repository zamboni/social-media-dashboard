from common import *
import sys

ROOT_URLCONF = 'social_media_dashboard.urls_development'

DASHBOARD_TEST_NAME = 'Vagrant dashboard'

INSTALLED_APPS += ('djcelery', )

BASE_URL = 'https://127.0.0.1:3060/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': '127.0.0.1',
        'NAME': 'social_media_planner',
        'USER': 'social_media_planner',
        'PASSWORD': 'social_media_planner',
    }
}

class DisableMigrations(object):

    def __contains__(self, item):
        return True

    def __getitem__(self, item):
        return "notmigrations"

TESTS_IN_PROGRESS = False
if 'test' in sys.argv[1:] or 'jenkins' in sys.argv[1:]:
    PASSWORD_HASHERS = (
        'django.contrib.auth.hashers.MD5PasswordHasher',
    )
    DEBUG = False
    TEMPLATE_DEBUG = False
    TESTS_IN_PROGRESS = True
    MIGRATION_MODULES = DisableMigrations()

'''
Celery '''
import djcelery
djcelery.setup_loader()

REDIS_HOST = "localhost"
REDIS_PORT = 6379
REDIS_PASSWORD = ""
BROKER_URL = 'redis://' + REDIS_HOST + ':' + str(REDIS_PORT) + '/6'
CELERY_RESULT_BACKEND = 'redis://' + REDIS_HOST + ':' + str(REDIS_PORT) + '/6'

from celery.schedules import crontab
CELERYBEAT_SCHEDULE = {
    # Executes every Monday morning at 7:30 A.M
    'send-mails': {
        'task': 'social_media_dashboard.tasks.send_all_mails',
        'schedule': crontab(minute='*/1'),
    },
}

try:
    from development_local import *
except:
    pass