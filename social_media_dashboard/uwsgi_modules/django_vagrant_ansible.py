import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.insert(1, os.path.abspath(os.path.join(os.path.dirname(__file__), "../")))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "social_media_dashboard.settings.vagrant_ansible")

application = get_wsgi_application()