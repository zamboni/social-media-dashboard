# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###


```
#!bash

git clone https://zamboni@bitbucket.org/zamboni/social-media-dashboard.git
cd social-media-dashboard/social_media_dashboard
pip install -r ../requirements.txt

python manage.py migrate
python manage.py shell_plus
```

Then, inside the shell, run:

```
#!python

from social_media_planner.fixtures_module import initial_features
initial_features.load_initial_features()
```


Create superuser from bash

```
#!bash

python manage.py createsuperuser
```

To send emails, install django-celery:


```
#!bash

pip install django-celery
```